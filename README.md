# project_2_test_artefacts

## Instructions

Напоминаем, что все отчёты по результатам выполнения заданий тебе придется оформлять в файлах с расширением `.md`. Все созданные отчёты и файлы тебе нужно будет загрузить в папку `src/` в корне проекта (обязательно в ветку _develop_). Если они уже созданы, то пересоздавать или удалять их не нужно (просто отредактируй этот файл).


<h3 id="задание-1-серый-ящик">Задание №1. Серый ящик</h3>

А что такое **"серый ящик"**? В каком случае он используется?🧐 Самостоятельно изучи эти вопросы. Ответы на них запиши в файл `exercise1.md`, предварительно его создав.

<h3 id="позитивное-и-негативное-тестирование">Позитивное и негативное тестирование</h3>

<h3 id="задание-2-список-тестовых-сценариев">Задание №2. Список тестовых сценариев</h3>

Перейди по ссылке https://www.saucedemo.com/. Ознакомься с функциональными возможностями демо-версии интернет-магазина. На странице авторизации снизу написан логин и пароль. Постарайся описать своими словами в файле `exercise2.md` (создай этот файл!) список сценариев позитивного тестирования так, чтобы они были как можно более исчерпывающими для проверки всех функциональных возможностей (также не забудь про страницу авторизации). В данном задании под сценариями тестирования понимается обычный набор действий, который необходимо выполнить, чтобы проверить отдельную функцию в приложении. Например:

```
Сценарий №1. Проверка успешного прохождения авторизации:
1. Перейти на страницу авторизации
2. В поле "Username" ввести "standard_user"
3. В поле "Password" ввести "secret_sauce"
4. Нажать на кнопку "LOGIN"
```

Как ты уже понял, мы рассматриваем только позитивные сценарии (не нужно проверять случай, когда мы вводим неправильный пароль из тысячи символов).

Полученный список помести под заголовок `# Список тестовых сценариев`.

_P.S.: список сценариев должен затрагивать только демонстрационный сайт "saucedemo"_

<h3 id="задание-3-тест-кейсы">Задание №3. Тест-кейсы</h3>

Создай файл `exercise3.md`. На основе сценариев, которые ты получил в результате выполнения задания №2, составь тест-кейсы (дополнив сценарий ожидаемым результатом, предшествующими условиями), пронумеруй каждый тест-кейс.

Пример тест-кейса:

```
## Тест-кейс №1 — Нажатие кнопки "Избранное"

1. Предшествующие условия:
    - пользователь авторизован
    - открыта новостная лента
2. Шаги:
    - навести курсор на запись
    - нажать на появившуюся кнопку "Избранное"
3. Ожидаемый результат:
    - при наведении появляется кнопка "Избранное"
    - при нажатии на кнопку "Избранное", запись добавляется в избранные публикации
    - к записи добавляется иконка "звёздочки"
```

<h3 id="задание-4-чек-лист">Задание №4. Чек-лист</h3>

В файле `exercise4.md` представлен шаблон будущего чек-листа и результаты его прогона. Кратко опиши в этом файле тест-кейсы, полученные в результате работы над прошлым заданием, и выполни каждый из них (вручную проверь [приложение](https://www.saucedemo.com/) на основании полученного чек-листа).

<h3 id="double-check">Double-check</h3>

Перед загрузкой выполненного проекта в репозиторий перепроверь наличие всех необходимых файлов, которые требовалось создать во время его выполнения:

```
exercise1.md
exercise2.md
exercise3.md
exercise4.md
```

